﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsData : MonoBehaviour {

	private const int MAX_VALUE = 500;
	private const int MIN_VALUE = 5;

	public int value;

	void Awake (){
		value = Random.Range (MIN_VALUE, MAX_VALUE);
	}


}
