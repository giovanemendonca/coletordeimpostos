﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	//Método que será executado quando o jogador clicar no botão de jogar
	//este método será vinculado ao botão de jogar na Scene MenuController
	public void StartGame (){

		SceneManager.LoadScene ("Main");
	}
}
