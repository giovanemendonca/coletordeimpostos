﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureData : MonoBehaviour {

	private const int MAX_VALUE = 500;
	private const int MIN_VALUE = 5;

	public int value;

	/** O metodo Awake é implementado pela classe MonoBehaviour
	 * e permite disponibilizar um valor para ser utilizado e acessado 
	 * por outros scripts.
	 **/
	void Awake (){
		//Método que vai atribuir um valor randomico entre 5 e 500
		value = Random.Range (MIN_VALUE, MAX_VALUE);
	}


}
