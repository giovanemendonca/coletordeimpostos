﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureHunter : MonoBehaviour {

	/**
	 * Classe responsável por executar ação de somar os valores das arcas quando 
	 * ocorrerem as colisões.
	 * Quando o motor de física do unity detectar colisão dos colladers do personagem e 
	 * da arca colidem executa este código.
	 * Classe adicionada ao personagem.
	 **/

	public TreasureHunterController controller;

	public void OnCollisionEnter (Collision collision) {

		if (collision.gameObject.tag.Equals ("Treasure")) {

			print ("Tesouro coletado! Valor = " + collision.gameObject.GetComponent<TreasureData>().value);
			//print ("Tesouro coletado! Valor = " + collision.gameObject.GetComponent<TreasureData> ().Value);
			controller.treasure += collision.gameObject.GetComponent<TreasureData>().value;
			Destroy (collision.gameObject);// destroi (apaga) do game a arca coletada.
		}
	}
}
