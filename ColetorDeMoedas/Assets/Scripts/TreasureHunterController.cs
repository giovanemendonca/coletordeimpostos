﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Adicionado (Importado) este package

public class TreasureHunterController : MonoBehaviour {

	/**
	 * Classe adicionada ao GameObject 'GameController'
	 * Vai guardar o somatório das arcas coletadas iniciando com 0 (zero).
	 **/

	public float treasure;
	public Text huntedTreasure;

	// variável pública da classe Time que será utilizada para exibir o tempo no visor do jogador
	public Text time;

	//variável pública do tipo float para guardar o tempo em segundos que será exibido
	public float timeInSeconds;

	//variável do tipo que executa a montagem/cria das arcas randomicamente
	public TreasureSpawner spawner;

	//Variável GameObject que refere-se ao panel criado para exibir após término do tempo
	public GameObject reiniciarAgora;

	//variável pública para armazenar a quantidade de pontos coletados
	public int numeroDeTesouros;

	private float tempoInicial;

	void Start() {

		//quando clicado no botão reiniciar vai setar na variável tempoInicial o valor do timeInSeconds por default.
		tempoInicial = timeInSeconds;

		treasure = 0;

		//Quando o iniciar o jogo exibir que o saldo de pontos das arcas está zerado 
		huntedTreasure.text = "R$ 0,00";

		//Inicializamos a variável time passando o timeInSeconds transformando em String 
		//para que não exiba as casas após a vírgula. 
		time.text = ((int)timeInSeconds).ToString();


	}


	/**
	 * Adicionarmos o Update (Aula: 6)
	 **/
	void Update () {

		//Quando executar o jogo e quando ocorrer o Update atualizar o saldo de arcas coletadas
		huntedTreasure.text = "R$ " + treasure;

		//Inicializamos a variável time passando o timeInSeconds transformando em String 
		//para que não exiba as casas após a vírgula. 
		time.text = ((int)timeInSeconds).ToString();
		timeInSeconds -= Time.deltaTime; // a classe deltaTime guarda valores em float

		if(timeInSeconds <= 0){	

			Time.timeScale = 0; //setamos timeScale como 0 para pausar o game indicando o fim do jogo.

			//após pausar o game na linha de código acima agora 
			//ativa o panel para exibir o botão de reiniciar
			reiniciarAgora.SetActive (true);

			//busca o gameObject com nome de 'txt_messagem' e seta no campo text a frase abaixo no unity.
			GameObject.Find ("txt_message").GetComponent<Text> ().text = "Parabéns! Você coletou = " + 
				huntedTreasure.text + "!";
		}
	}

	/**
	 * Método executado quando o jogador clicar no button de reiniciar
	 **/

	public void Reiniciar() {

		//resetar o tempo inicial
		timeInSeconds = tempoInicial;
		//resetar o treasure para 0;
		treasure = 0;

		time.text = ((int)timeInSeconds).ToString();
		huntedTreasure.text = "R$ 0,00";

		//Passando o timeScale para 1 serve para despausar o game.
		Time.timeScale = 1;

		//elima-esconde o panel de reinicialização.
		reiniciarAgora.SetActive(false);

		//verifica todas as arcas restantes que não foram coletadas
		//faz a busca dos GameObjects que possuem a tag 'Treasure'
		var treasureChests = GameObject.FindGameObjectsWithTag ("Treasure");
		//apartir deste trecho destroi as arcas encontradas que não foram coletadas.
		foreach (var chest in treasureChests) 
			Destroy (chest);
		

		//serve para remontar as arcas de tesouro na tela no jogador
		spawner.Spawn (numeroDeTesouros);

	}

}
