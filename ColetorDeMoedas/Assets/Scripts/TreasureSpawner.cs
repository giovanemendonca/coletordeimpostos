﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureSpawner : MonoBehaviour {

	//Variável treasure que será o GameObject
	public GameObject treasure;

	void Start (){
		//Indicamos que o jogo terá 200 baús randomicos.
		Spawn (200);
	}

	public void Spawn (int numberOfTreasure){

		while (numberOfTreasure != 0){
			/** utilizamos o Vector3 para posicionar as arcas em locais randomicos conforme método Random.Rage
			 * a posição de Y é fixada em 250 para que a arca seja posicionada na superfície do terrain, 
			 * caso contrário poderia ser posicionada em qualquer lugar no espaço acima ou abaixo do terrain.
			 **/
			//passamos o GameObject 'treasure', passamos uma posição no espaço usando Vector3
			//passamos a rotação do GameObject fixa sem alterar valores.
			Instantiate (treasure, new Vector3 (Random.Range (0, 2000), 250, Random.Range (0, 2000)), 
				treasure.transform.rotation);

			numberOfTreasure--; //A cada loop do while reduz o número do numberOfTreasure até atingir 200
		}
	}
}
