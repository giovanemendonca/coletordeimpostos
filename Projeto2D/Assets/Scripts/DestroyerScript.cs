﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Este script, quando anexado ao objeto Destroyer será responsável por 
 * destruir objetos que foram criados na cena e não são mais utilizados no jogo;
 **/

public class DestroyerScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
