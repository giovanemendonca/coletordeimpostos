﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe Player
/// </summary>
public class PlayerScript : MonoBehaviour
{
	// Variável Vector2 utilizada para calcular a velocidade da nave no eixos X,Y
	public Vector2 speed = new Vector2(0, 0);
	
	// Variável Vector2 que armazena o movimento da nave no eixos X,Y
	private Vector2 movement;

	// O método Update é executado a cada frame do jogo
	void Update()
	{
		// Movimenta o player para CIMA/BAIXO
		MovePlayer();
		
		// Calcula o limite do jogador na tela, 
		// não deixando sair da visão da câmera
		CalculesLimitOfThePlayerOnTheScreen();
	}

	// Movimenta o player para CIMA/BAIXO
	void MovePlayer()
	{
		// Checa se um touch foi reconhecido, caso positivo a nave 
		// pode ser controlado por toques
		// Neste caso provavelmente trata-se de um dispositivo 
		// móvel (smartphones e tablets)
		if (Input.touchCount > 0) 
		{
			// Armazena o toque
			Touch touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began || 
			    touch.phase == TouchPhase.Moved || 
			    touch.phase == TouchPhase.Stationary) 
			{
				// Armazena a posição exata do toque que será 
				// usado para direcionar a nave
				Vector2 deltaPosition = Input.GetTouch(0).deltaPosition;
				
				// Calcula o movimento da nave, levando em consideração a velocidade 
				// no eixo (X,Y) * 2 * posição do toque
				movement = new Vector2(
					speed.x * 2.0f * deltaPosition.x * Time.deltaTime, 
					speed.y * 2.0f * deltaPosition.y * Time.deltaTime
					);
			}
		}
		
		// Caso o touch não tenha sido reconhecido, utilizaremos o keyboard 
		// para controle da nave
		else
		{
			// Pega o Input através do teclado no eixo X através da Horizontal 
			// (Teclas A / D e Seta Esquerda / Seta Direita
			float inputX = Input.GetAxis("Horizontal");
			
			//Pega o Input através do teclado no eixo Y através da Vertical
			// (Teclas W / S e Seta para Cima / Seta pra Baixo
			float inputY = Input.GetAxis("Vertical");
			
			// Calcula o movimento da nave, levando em consideração 
			//para cada eixo (X,Y) a velocidade * input do teclado
			movement = new Vector2(speed.x * inputX, speed.y * inputY);
		}     
	}

	// Calcula o limite do jogador na tela, 
	// não deixando sair da visão da câmera
	void CalculesLimitOfThePlayerOnTheScreen ()
	{
		float margin = 2.0f;
		
		// Controle para a nave não sair fora da tela (superior e inferior)
		// Calcula a distância do player em relação a câmera
		var distance = (transform.position - Camera.main.transform.position).z;
		
		// Borda superior
		var topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).y;
		topBorder = topBorder + margin;
		
		// Borda inferior
		var bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distance)).y;
		bottomBorder = bottomBorder - margin;
		
		// Controla a posição do player entre o canto superior e inferior
		float posY = Mathf.Clamp(transform.position.y, topBorder, bottomBorder);
		transform.position = new Vector3(transform.position.x, posY, transform.position.z);
	}
	
	// O método FixedUpdate é executado a cada frame do jogo, 
	// e deve ser usado para objetos que tenham física (rigidbody)
	void FixedUpdate()
	{
		// Movimenta o objeto através da velocidade
		rigidbody2D.velocity = movement;
	}
}
